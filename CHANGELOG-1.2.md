# Change log


## v1.2.1 (2018-09-05)


### Changed

- Package name changed from ``roumen/disqus`` to ``laravelium/disqus`` ([new packagist repo](https://packagist.org/packages/laravelium/disqus))

### Added

- Added new [packagist repo](https://packagist.org/packages/laravelium/disqus)
- Added new branch for development ``1.2.x-dev``
- Added new [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) testing with code coverage
- Added new coding style testing with [StyleCI](https://gitlab.styleci.io/repos/8088383)
- Added release changelog
- Added contributing guidelines
- Added support for Laravel 5.7

### Fixed

- Minor bug fixes and optimizations
