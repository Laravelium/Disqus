<?php namespace Laravelium\Disqus;

use Illuminate\Support\ServiceProvider;
use Laravelium\Disqus\Disqus;

class DisqusServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('disqus', function ($app, $params) {
            return new Disqus($params);
        });

        $this->app->alias('disqus', Disqus::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['disqus', Disqus::class];
    }
}
