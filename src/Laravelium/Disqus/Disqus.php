<?php namespace Laravelium\Disqus;

/**
 * Laravelium Disqus package for Laravel.
 *
 * @author Roumen Damianoff <roumen@damianoff.com>
 * @version 2.0
 * @link https://laravelium.com
 * @license http://opensource.org/licenses/mit-license.php MIT License
 */

require_once('vendor/disqus/disqus-php/disqusapi/disqusapi.php');

class Disqus extends \DisqusAPI
{
    // wip
}
