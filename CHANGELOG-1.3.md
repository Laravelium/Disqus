# Change log


## v1.3.1 (2018-09-04)

### Added

- Added support to Laravel 5.8
- Added new branch for development ``1.3.x-dev``

### Fixed

- Minor bug fixes and optimizations
