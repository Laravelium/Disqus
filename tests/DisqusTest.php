<?php namespace Laravelium\Disqus\Test;

use Orchestra\Testbench\TestCase as TestCase;
use Laravelium\Disqus\Disqus;
use Laravelium\Disqus\DisqusServiceProvider;
use Carbon\Carbon;

class DisqusTest extends TestCase
{
    protected $disqus;
    protected $sp;

    protected function getPackageProviders($app)
    {
        return [DisqusServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return ['Disqus' => Disqus::class];
    }

    public function setUp() : void
    {
        parent::setUp();

        $this->disqus = $this->app->make(Disqus::class, ['secretKey']);
    }

    public function testMisc()
    {
        // test object initialization
        $this->assertInstanceOf(Disqus::class, $this->disqus);

        // test custom methods
        $this->assertEquals([DisqusServiceProvider::class], $this->getPackageProviders($this->disqus));
        $this->assertEquals(['Disqus'=>Disqus::class], $this->getPackageAliases($this->disqus));

        // test DisqusServiceProvider (fixes coverage of the class!)
        $this->sp = new DisqusServiceProvider($this->disqus);
        $this->assertEquals(['disqus', Disqus::class], $this->sp->provides());
    }

    public function testDisqusAPIException()
    {
        // with invalid/unset key (expected exception!)
        $this->expectException(\DisqusAPIError::class);
        $this->disqus->trends->listThreads(['forum' => null, 'limit' => 2]);
    }

    public function testDisqusAPISuccess()
    {
        // with valid key (no exceptions!)
        $this->disqus->setKey('Ls5JaWWVGh0HDoALcaPUUxb7pkBTcMFj43fa7erRH2NGDeqVxolgv7yrJECKufka');
        $this->assertCount(4, $this->disqus->trends->listThreads(['forum' => null, 'limit' => 4]));
        $this->assertCount(2, $this->disqus->trends->listThreads(['forum' => null, 'limit' => 2]));
    }
}
