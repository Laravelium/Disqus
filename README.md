# **[Laravelium Disqus](https://laravelium.com) package**

*Laravelium Disqus package for Laravel.*

## Notes

- Dev Branches are for development and are **UNSTABLE** (*use on your own risk!*)
- Because `disqus-php` is not a valid composer package you must add it as custom repository your root `composer.json`

## Installation

### Add this custom repository (in `repositories` section) to your `composer.json` file :

```json
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "disqus/disqus-php",
            "version": "dev-master",
            "source": {
                "url": "https://github.com/disqus/disqus-php.git",
                "type": "git",
                "reference": "master"
            }
        }
    }],
```

### Add the following to `require` section in your `composer.json` file :

#### For Laravel 5.8
```json
"laravelium/disqus": "1.3.*",
"disqus/disqus-php": "dev-master"
```
or (development branch)
```json
"laravelium/disqus": "1.3.x-dev",
"disqus/disqus-php": "dev-master"
```

#### For Laravel 5.7
```json
"laravelium/disqus": "1.2.*",
"disqus/disqus-php": "dev-master"
```
or (development branch)
```json
"laravelium/disqus": "1.2.x-dev",
"disqus/disqus-php": "dev-master"
```

#### For Laravel 5.6
```json
"laravelium/disqus": "1.1.*",
"disqus/disqus-php": "dev-master"
```
or (development branch)
```json
"laravelium/disqus": "1.1.x-dev",
"disqus/disqus-php": "dev-master"
```

#### For Laravel 5.5
```json
"laravelium/disqus": "1.0.*",
"disqus/disqus-php": "dev-master"
```
or (development branch)
```json
"laravelium/disqus": "1.0.x-dev",
"disqus/disqus-php": "dev-master"
```

## Examples

- [How to sync comments from Disqus to your local DB](https://gitlab.com/Laravelium/Disqus/wikis/Sync-comments)

and more in the [Wiki](https://gitlab.com/Laravelium/Disqus/wikis/home).

## Contribution guidelines

Before submiting new merge request or creating new issue, please read [contribution guidelines](https://gitlab.com/Laravelium/Disqus/blob/master/CONTRIBUTING.md).

## License

This package is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).